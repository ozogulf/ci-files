#!/bin/sh

APPNAME=""
DISTRO=""
EMAIL=""
USERNAME=""
VERSION=""
DATE=`LANG=C date -R`

DEBIAN_PATH=$PWD/DEBIAN_FILES/

change_control() {
    cp $DEBIAN_PATH/control $TARGET_PATH/DEBIAN;
    cd $TARGET_PATH/DEBIAN;
    sed -i "s/e_mail/$EMAIL/g" control
    sed -i "s/app_name/$APPNAME/g" control
    sed -i "s/user_name/$USERNAME/g" control
    sed -i "s/app_version/$VERSION/g" control
    cd $DEBIAN_PATH
}

change_changelog() {
    cp $DEBIAN_PATH/changelog $TARGET_PATH/DEBIAN; 
    cd $TARGET_PATH/DEBIAN;
    sed -i "s/e_mail/$EMAIL/g" changelog
    sed -i "s/app_name/$APPNAME/g" changelog
    sed -i "s/app_distro/$DISTRO/g" changelog
    sed -i "s/created_date/$DATE/g" changelog
    sed -i "s/user_name/$USERNAME/g" changelog
    sed -i "s/app_version/$VERSION/g" changelog
    cd $DEBIAN_PATH
}

change_copyright() {
    cp $DEBIAN_PATH/copyright $TARGET_PATH/DEBIAN; 
    cd $TARGET_PATH/DEBIAN;
    sed -i "s/app_name/$APPNAME/g" copyright
    cd $DEBIAN_PATH
}

# a = appname
# d = distro
# e = email
# n = username
# v = version
while getopts a:d:e:u:v: option
do
case "${option}"
in
    a) APPNAME=${OPTARG}
        ;;
    d) DISTRO=${OPTARG}
        ;;
    e) EMAIL=$OPTARG
        ;;
    u) USERNAME=${OPTARG}
        ;;
    v) VERSION=$OPTARG
        ;;
esac
done

echo "Application name" $APPNAME
echo "Distro" $DISTRO
echo "Email" $EMAIL
echo "Version" $VERSION
echo "Date" $DATE
echo "~~~~~~~~~"

TARGET_PATH=$PWD/"$APPNAME"_"$VERSION"
mkdir -p $TARGET_PATH/DEBIAN

change_control
change_changelog
change_copyright
cp $DEBIAN_PATH/compat $TARGET_PATH/DEBIAN
